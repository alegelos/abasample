//
//  SearchMediaViewModelTest.swift
//  ABASampleTests
//
//  Created by Alejandro Gelos on 12/10/18.
//  Copyright © 2018 Alejandro Gelos. All rights reserved.
//

import XCTest
@testable import ABASample

class SearchMediaViewModelTest: XCTestCase {

    private var viewModel: SearchMediaViewModel!
    private var service:   MediaServicesManagerProtocol!
    
    override func setUp() {
        super.setUp()
        service = MediaServicesManagerMock()
        viewModel = SearchMediaViewModel(iTunesManager: ITunesServicesManagerMock())
    }
    
    override func tearDown() {
        service   = nil
        viewModel = nil
        super.tearDown()
    }
    
    func testUpdate() {
        // given empty result and a search
        XCTAssertTrue(viewModel.results.value.isEmpty)
        viewModel.update(searchText: "test")
        
        // expected results not empty
        XCTAssertFalse(viewModel.results.value.isEmpty)
    }
    
    func testSortButtonPressed(){
        // given a default sort type and change to next one
        let prevoisSortType = viewModel.sortType.value
        viewModel.sortButtonPressed()
        
        // expected sort type to be different from default one
        XCTAssertNotEqual(prevoisSortType, viewModel.sortType.value)
        
        // expected sort type to be different from default one
        viewModel.sortButtonPressed()
        XCTAssertNotEqual(prevoisSortType, viewModel.sortType.value)
        
        // expected sort type to loop and be default one
        viewModel.sortButtonPressed()
        XCTAssertEqual(prevoisSortType, viewModel.sortType.value)
    }
}
