//
//  MediPlayerViewModelTest.swift
//  ABASampleTests
//
//  Created by Alejandro Gelos on 12/9/18.
//  Copyright © 2018 Alejandro Gelos. All rights reserved.
//

import XCTest
@testable import ABASample

class MediPlayerViewModelTest: XCTestCase {

    private var mediaPlayer: MediaPlayer!
    private var viewModel:   MediaPlayerViewModel!
    private var service:     MediaServicesManagerProtocol!
    
    private var iTunesServicesManagerMock: ITunesServicesManagerMock!
    
    override func setUp() {
        super.setUp()
        iTunesServicesManagerMock = ITunesServicesManagerMock()
        service = MediaServicesManagerMock()
        mediaPlayer = MediaPlayerManager.shared
        viewModel = MediaPlayerViewModel(service, mediaPlayer)
        
        iTunesServicesManagerMock.search(forText: ""){ [unowned self] result in
            switch result{
            case .success(let tracks, _):
                self.viewModel.results.value = tracks
            default:
                break
            }
        }
    }
    
    override func tearDown() {
        service   = nil
        viewModel = nil
        iTunesServicesManagerMock = nil
        super.tearDown()
    }
    
    func testControllerDidLoad(){
        // giving viewModel loaded with tracks
        viewModel.controllerDidLoad()
        
        // expected tracks loaded in MediaPlayerManager
        XCTAssertEqual(MediaPlayerManager.shared.getMediaList(), viewModel.results.value)
    }
    
    func testNextButtonPressed(){
        // giving viewModel current track equal to 10. MediaPlayer loaded with tracks
        mediaPlayer.set(mediaList: viewModel.results.value)
        viewModel.currentTrack.value = 10
        
        viewModel.nextButtonPressed()
        
        // expected viewModel current track equals to 11
        XCTAssertEqual(viewModel.currentTrack.value, 11)
    }
    
    func testNextButtonPrevious() {
        // giving viewModel current track equal to 10. MediaPlayer loaded with tracks
        mediaPlayer.set(mediaList: viewModel.results.value)
        viewModel.currentTrack.value = 10
        
        viewModel.previousButtonPressed()
        
        // expected viewModel current track equals to 9
        XCTAssertEqual(viewModel.currentTrack.value, 9)
    }
    
    func testShareButtonPressed(){
        // giving current track euqal to 10
        viewModel.currentTrack.value = 10
        
        viewModel.shareButtonPressed()
        
        // expected viewModel previewToShare value not nil
        XCTAssertNotNil(viewModel.previewToShare.value)
    }
    
    func testDidScroll() {
        // given scrolled from item 9 to item 10. MediaPlayer loaded with tracks
        mediaPlayer.set(mediaList: viewModel.results.value)
        viewModel.currentTrack.value = 9
        viewModel.didScroll(toItem: 10)
        
        // expected to change song if scrolled to different item
        XCTAssertEqual(10, MediaPlayerManager.shared.getCurrentTrack())
    }
    
    func testDidScrollToSameItem(){
        // given scrolled from item 10 to item 10. MediaPlayer loaded with tracks
        mediaPlayer.set(mediaList: viewModel.results.value)
        viewModel.didScroll(toItem: 9)
        viewModel.currentTrack.value = 10
        viewModel.didScroll(toItem: 10)
        
        // expected not to change song if scrolled to same item
        XCTAssertEqual(9, MediaPlayerManager.shared.getCurrentTrack())
    }
    
    func testCloseButtonPressed() {
        // given close button pressed
        viewModel.closeButtonPressed()
        
        // expected close button value to be true
        XCTAssertTrue(viewModel.closeAction.value)
    }
    
    func testCollectionDidReload() {
        // given current track 10. MediaPlayer loaded with tracks
        mediaPlayer.set(mediaList: viewModel.results.value)
        viewModel.currentTrack.value = 10
        viewModel.collectionDidReload()
        
        // expected to play track 10 when reload
        XCTAssertEqual(10, MediaPlayerManager.shared.getCurrentTrack())
    }
    
    func testDidFinishPlaying() {
        // given playing song to true and close action to false
        viewModel.isPlayingTrack.value = true
        viewModel.closeAction.value = false
        
        viewModel.didFinishPlaying()
        
        // expected playing song to false and close action to true
        XCTAssertTrue(viewModel.closeAction.value)
        XCTAssertFalse(viewModel.isPlayingTrack.value)
    }
    
    func testNowPlaying() {
        // given current track 10. MediaPlayer loaded with tracks. IsPlaying set to false
        mediaPlayer.set(mediaList: viewModel.results.value)
        viewModel.currentTrack.value = 10
        viewModel.isPlayingTrack.value = false
        
        viewModel.nowPlaying(media: 11)

        // expected current track to be 11 and isPlayingTrack set to true
        XCTAssertEqual(viewModel.currentTrack.value, 11)
        XCTAssertTrue(viewModel.isPlayingTrack.value)
    }
    
    func testPerformanceDecodingTracks() {
        self.measure {
            iTunesServicesManagerMock.search(forText: "") {retuels in }
        }
    }
}
