//
//  MediaServicesManagerMock.swift
//  ABASampleTests
//
//  Created by Alejandro Gelos on 12/9/18.
//  Copyright © 2018 Alejandro Gelos. All rights reserved.
//

import XCTest
@testable import ABASample

class MediaServicesManagerMock: MediaServicesManagerProtocol {
    private let imageCache      = NSCache<NSString, UIImage>()
    var coverImageUrl: String?
    
    func getImage(fromUrl url: String, completion: @escaping (Result<UIImage, String, ErrorResult>) -> Void) {
        let image = UIImage()
        let url = "test"
        
        imageCache.setObject(image, forKey: url as NSString)
        completion(.success(image, url))
    }
    
    func isResponseValid(askedUrl: String?, imageUrl: String) -> Bool {
        guard let askedUrl = askedUrl else {return false}
        return askedUrl == imageUrl
    }
    
    func cleanCache() {
        imageCache.removeAllObjects()
    }
}
