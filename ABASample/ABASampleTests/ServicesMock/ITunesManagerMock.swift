//
//  ITunesManagerMock.swift
//  ABASampleTests
//
//  Created by Alejandro Gelos on 12/9/18.
//  Copyright © 2018 Alejandro Gelos. All rights reserved.
//

import XCTest
@testable import ABASample

class ITunesServicesManagerMock: ITunesServicesManagerProtocol {
    let searchCache = NSCache<NSString, StructWrapper<([Track], URLResponse)>>()
    
    func cleanCache() {
        searchCache.removeAllObjects()
    }
    
    func search(forText text: String, completion: @escaping (Result<[Track], URLResponse, ErrorResult>) -> Void) {
        let filePath = Bundle(for: type(of: self)).path(forResource: "SearchResultsDummy", ofType: "json")
        let data = try! Data(contentsOf: URL(fileURLWithPath: filePath!), options: .mappedIfSafe)
        
        let response = URLResponse(url: URL(string: "https:www.test.com")!, mimeType: nil, expectedContentLength: 0, textEncodingName: nil)
        let searchText = "Michael Jackson"
        do {
            let apiResponse = try JSONDecoder().decode(ITunesApiResponse.self, from: data)
            searchCache.setObject(StructWrapper((apiResponse.results, response)), forKey: searchText as NSString)
            completion(.success(apiResponse.results, response))
        } catch {
            completion(.failure(.network(string: NetworkResponse.unableToDecode.rawValue)))
        }
    }
}
