//
//  SorterUtilsTest.swift
//  ABASampleTests
//
//  Created by Alejandro Gelos on 12/10/18.
//  Copyright © 2018 Alejandro Gelos. All rights reserved.
//

import XCTest
@testable import ABASample

class SorterUtilsTest: XCTestCase {

    private var sorterUtils: SorterUtils!
    private var iTunesServicesManagerMock: ITunesServicesManagerMock!
    private var mediaTracks = [Track]()
    
    override func setUp() {
        super.setUp()
        sorterUtils = SorterUtils()
        iTunesServicesManagerMock = ITunesServicesManagerMock()
        
        iTunesServicesManagerMock.search(forText: "") { [unowned self] result in
            switch result {
            case .success(let tracks, _):
                self.mediaTracks = tracks
            default:
                break
            }
        }
    }

    override func tearDown() {
        sorterUtils = nil
        iTunesServicesManagerMock = nil
        super.tearDown()
    }
    
    func testSortDuration() {
        // given a random order array of tracks
        let sortTracks = sorterUtils.sort(tracks: &mediaTracks, by: .Duration)
        
        var isSorted = true
        for i in 1..<sortTracks.count {
            if let value1 = sortTracks[i-1].trackTimeMillis, let value2 = sortTracks[i].trackTimeMillis, value1 < value2 {
                isSorted = false
            }
        }
        // expected sortTracks to be sorted by duration
        XCTAssertTrue(isSorted)
    }
    
    func testSortGenre() {
        // given a random order array of tracks
        let sortTracks = sorterUtils.sort(tracks: &mediaTracks, by: .Genre)
        
        var isSorted = true
        for i in 1..<sortTracks.count {
            if let value1 = sortTracks[i-1].primaryGenreName,  let value2 = sortTracks[i].primaryGenreName, value1 > value2 {
                isSorted = false
            }
        }
        // expected sortTracks to be sorted by genre
        XCTAssertTrue(isSorted)
    }
    
    func testSortPrice() {
        // given a random order array of tracks
        let sortTracks = sorterUtils.sort(tracks: &mediaTracks, by: .Price)
        
        var isSorted = true
        for i in 1..<sortTracks.count {
            if let value1 = sortTracks[i-1].trackPrice, let value2 = sortTracks[i].trackPrice, value1 < value2 {
                isSorted = false
            }
        }
        // expected sortTracks to be sorted by price
        XCTAssertTrue(isSorted)
    }


    func testPerformanceExample() {
        self.measure {
            _ = sorterUtils.sort(tracks: &mediaTracks, by: .Duration)
        }
    }
    
    func testPerformanceExample2() {
        self.measure {
            _ = sorterUtils.sort(tracks: &mediaTracks, by: .Genre)
        }
    }
    
    func testPerformanceExample3() {
        self.measure {
            _ = sorterUtils.sort(tracks: &mediaTracks, by: .Price)
        }
    }

}
