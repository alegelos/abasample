//
//  TimeUtils.swift
//  gGmbH
//
//  Created by Alejandro Gelos on 12/7/18.
//  Copyright © 2018 Alejandro Gelos. All rights reserved.
//

import Foundation

/**
 Returns a String with the time with a clock format
 - Parameters:
 - fromMilli: time in milliseonds
 
 - Returns: String with the format h:mm:ss or mm:ss if hours are 0
 */
struct TimeUtils {
    func getFormatedTime(fromMilli milli: Double) -> String {
        let secondsInt = (Int(milli)/1000)%60
        let minutsInt  = (Int(milli)/60000)%60
        let hour    = Int(milli/3600000)
        let hourString = hour > 0 ? String(hour)+":" : ""
        
        var seconds = String(secondsInt)
        if secondsInt < 10{
            seconds = "0" + seconds
        }
        
        var minuts = String(minutsInt)
        if !hourString.isEmpty, minutsInt < 10{
            minuts = "0" + minuts
        }
        
        return hourString + minuts + ":" + seconds
    }
}
