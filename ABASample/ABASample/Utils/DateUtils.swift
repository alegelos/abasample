//
//  DateUtils.swift
//  gGmbH
//
//  Created by Alejandro Gelos on 12/6/18.
//  Copyright © 2018 Alejandro Gelos. All rights reserved.
//

import Foundation

struct DateUtils {
    static let dateFormater = ISO8601DateFormatter()
    static let dateFormaterToString = DateFormatter()
    
    func getDate(fromString dateString:String?) -> Date? {
        guard let dateString = dateString else{return nil}
        return DateUtils.dateFormater.date(from: dateString)
    }
    
    func getYear(fromDate date:Date?, withFormat format:String = "yyyy") -> String?{
        guard let date = date else {return nil}
        DateUtils.dateFormaterToString.dateFormat = format
        return DateUtils.dateFormaterToString.string(from: date)
    }
    
    func getYear(fromString dateString:String?) -> String? {
        guard let dateString = dateString, let date = getDate(fromString: dateString)else {return nil}
        return getYear(fromDate: date)
    }
}
