//
//  MediaPlayerViewModel.swift
//  gGmbH
//
//  Created by Alejandro Gelos on 12/7/18.
//  Copyright © 2018 Alejandro Gelos. All rights reserved.
//

import Foundation

final class MediaPlayerViewModel {
    var results: Box<[Track]> = Box([Track]())
    var currentTrack   = Box(0)
    var isPlayingTrack = Box(true)
    var closeAction    = Box(false)
    var previewToShare: Box<String?> = Box(nil)
    
    private let mediaNetworking: MediaServicesManagerProtocol!
    private let mediaPlayerManager: MediaPlayer!
    
    init(_ mediaNetworking: MediaServicesManagerProtocol = MediaServicesManager(), _ mediaPlayer: MediaPlayer = MediaPlayerManager.shared) {
        self.mediaNetworking = mediaNetworking
        
        mediaPlayerManager = mediaPlayer
        mediaPlayerManager.set(delegate: self)
    }
    
    func controllerDidLoad() {
        mediaPlayerManager.set(mediaList: results.value)
    }
    
    func playButtonPressed() {
        switch mediaPlayerManager.isPlayingMedia {
        case .some(.paused):
            mediaPlayerManager.play()
            isPlayingTrack.value = true
        case .some(.playing):
            mediaPlayerManager.pauseMedia()
            isPlayingTrack.value = false
        case .some(.waitingToPlayAtSpecifiedRate):
            guard !isPlayingTrack.value else {return}
            mediaPlayerManager.playMedia(item: currentTrack.value)
        default:
            mediaPlayerManager.playMedia(item: currentTrack.value)
        }
    }
    
    func nextButtonPressed() {
        mediaPlayerManager.playMedia(item: currentTrack.value + 1)
    }
    
    func previousButtonPressed() {
        mediaPlayerManager.playMedia(item: currentTrack.value - 1)
    }
    
    func shareButtonPressed() {
        guard let preview = results.value[currentTrack.value].previewUrl else {
            print(gGmbHErrors.failToShareItem.localizedDescription)
            return
        }
        previewToShare.value = preview
    }
    
    func didScroll(toItem item: Int) {
        guard item != currentTrack.value else {
            currentTrack.value = item
            return
        }
        mediaPlayerManager.playMedia(item: item)
    }
    
    func closeButtonPressed() {
        closeAction.value = true
    }
    
    func didReceiveMemoryWarning() {
        mediaNetworking.cleanCache()
    }
    
    func collectionDidReload() {
        mediaPlayerManager.playMedia(item: currentTrack.value)
    }
    
    func willResignActiveNotification() {
        didScroll(toItem: currentTrack.value)
    }
}

// MARK: - MediaPlayerManagerDelegate
extension MediaPlayerViewModel: MediaPlayerManagerDelegate {
    func didFinishPlaying() {
        isPlayingTrack.value = false
        closeAction.value = true
    }
    
    func nowPlaying(media: Int) {
        currentTrack.value = media
        if !isPlayingTrack.value {
            isPlayingTrack.value = true
        }
    }
}
