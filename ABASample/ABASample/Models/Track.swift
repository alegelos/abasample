//
//  Track.swift
//  gGmbH
//
//  Created by Alejandro Gelos on 12/9/18.
//  Copyright © 2018 Alejandro Gelos. All rights reserved.
//

import Foundation

//If more decodable will be needed, change struct to class and subclass.
struct Track:Equatable {
    let trackId: Int?
    let collectionId: Int?
    let releaseDate: Date?
    let trackName: String?
    let trackTime: String?
    let artistName: String?
    let trackPrice: Double?
    let previewUrl: String?
    let releaseYear: String?
    let artworkUrl30: String?
    let artworkUrl60: String?
    let artworkUrl100: String?
    let collectionName: String?
    let trackTimeMillis: Double?
    let primaryGenreName: String?
    let trackCensoredName: String?
    let collectionArtistName: String?
    let collectionCensoredName: String?
}
