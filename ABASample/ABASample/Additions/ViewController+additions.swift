//
//  ViewController+additions.swift
//  gGmbH
//
//  Created by Alejandro Gelos on 12/7/18.
//  Copyright © 2018 Alejandro Gelos. All rights reserved.
//

import UIKit

extension UIViewController {
    func share(withTitle title: String = "Check out this media track!!",_ link: String) {
        let activityVC = UIActivityViewController(activityItems: [title, link], applicationActivities: nil)
        present(activityVC, animated: true, completion: nil)
    }
}
