//
//  Notifications+additions.swift
//  ABASample
//
//  Created by Alejandro Gelos on 12/11/18.
//  Copyright © 2018 Alejandro Gelos. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let willPlayMedia = Notification.Name("willPlayMedia")
}
