//
//  StructWrapper.swift
//  gGmbH
//
//  Created by Alejandro Gelos on 12/5/18.
//  Copyright © 2018 Alejandro Gelos. All rights reserved.
//

import Foundation

final class StructWrapper<T>: NSObject {
    
    let value: T
    
    init(_ _struct: T) {
        self.value = _struct
    }
}
