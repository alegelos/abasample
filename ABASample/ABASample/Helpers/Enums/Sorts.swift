//
//  Sorts.swift
//  gGmbH
//
//  Created by Alejandro Gelos on 12/6/18.
//  Copyright © 2018 Alejandro Gelos. All rights reserved.
//

import Foundation

enum Sorts: String {
    case Price, Duration, Genre
    
    func getNextSortType() -> Sorts {
        switch self {
        case .Genre:    return Sorts.Price
        case .Price:    return Sorts.Duration
        case .Duration: return Sorts.Genre
        }
    }
}
