//
//  gGmbHErrors.swift
//  gGmbH
//
//  Created by Alejandro Gelos on 12/4/18.
//  Copyright © 2018 Alejandro Gelos. All rights reserved.
//

import Foundation

enum gGmbHErrors:Error {
    case failtToDequeCell, failToShareItem, failtToDownloadSampleTrack, failtToGetCoverTrack, trackOutOfMediaListBounds
}
