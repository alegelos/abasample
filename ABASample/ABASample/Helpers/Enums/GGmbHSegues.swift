//
//  GGmbHSegues.swift
//  gGmbH
//
//  Created by Alejandro Gelos on 12/7/18.
//  Copyright © 2018 Alejandro Gelos. All rights reserved.
//

import Foundation

enum GGMBHSegues:String {
    case GoToMediaPlayer
}
