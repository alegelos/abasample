//
//  LoadingIndicator.swift
//  gGmbH
//
//  Created by Alejandro Gelos on 12/6/18.
//  Copyright © 2018 Alejandro Gelos. All rights reserved.
//

import UIKit

final class LoadingIndicator: UIActivityIndicatorView {

    override func stopAnimating() {
        DispatchQueue.main.async {
            super.stopAnimating()
        }
    }
    
    override func startAnimating() {
        DispatchQueue.main.async {
            super.startAnimating()
        }
    }
}
