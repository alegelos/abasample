//
//  MovieEndPoint.swift
//  NetworkLayer
//
//  Created by Alejandro Gelos on 12/2/18.
//  Copyright © 2018 Alejandro Gelos. All rights reserved.
//

import Foundation

enum ITunesNetworkEnvironment {
    case qa
    case production
    case staging
}

enum ITunesApi {
    case search(for:String)
}

extension ITunesApi: EndPointType {
    
    var environmentBaseURL: String {
        switch ITunesServicesManager.environment {
        case .production: return "https://itunes.apple.com"
        case .qa:         return "https://itunes.apple.com"
        case .staging:    return "https://itunes.apple.com"
        }
    }
    
    var baseURL: URL {
        guard let url = URL(string: environmentBaseURL) else { fatalError("baseURL could not be configured.")}
        return url
    }
    
    var path: String {
        switch self {
        case .search:
            return "/search"
        }
    }
    
    var httpMethod: HTTPMethod {
        return .get
    }
    
    var task: HTTPTask {
        switch self {
        case .search(let text):
            return .requestParameters(bodyParameters: nil,
                                      bodyEncoding: .urlEncoding,
                                      urlParameters: ["term":text, "media":"musicVideo", "entity":"musicVideo", "attribute":"artistTerm", "limit":"200"])
        }
    }
    
    var headers: HTTPHeaders? {
        return nil
    }
}
