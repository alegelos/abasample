//
//  ITunesUtils.swift
//  gGmbH
//
//  Created by Alejandro Gelos on 12/3/18.
//  Copyright © 2018 Alejandro Gelos. All rights reserved.
//

import Foundation

struct ITunesUtils {
    func prepareTextForRequest(text: String) -> String {
        return text.trimmingCharacters(in: .whitespacesAndNewlines).replacingOccurrences(of: " ", with: "+")
    }
    
    func isSearchResponseValid(searchText text:String?, response: URLResponse) -> Bool {
        guard let text = text, let url = response.url else {return false}
        return url.absoluteString.contains(prepareTextForRequest(text: text))
    }
}
