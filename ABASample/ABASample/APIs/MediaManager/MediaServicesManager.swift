//
//  MediaManager.swift
//  NetworkLayer
//
//  Created by Alejandro Gelos on 12/2/18.
//  Copyright © 2018 Alejandro Gelos. All rights reserved.
//

import UIKit

protocol MediaServicesManagerProtocol {
    func getImage(fromUrl url: String, completion: @escaping (Result<UIImage, String, ErrorResult>) -> Void)
    func isResponseValid(askedUrl: String?, imageUrl: String) -> Bool
    func cleanCache()
}

struct MediaServicesManager {
    let router: DirectRoute!
    
    private let imageCache      = NSCache<NSString, UIImage>()
    
    init(withRouter router: DirectRoute = DirectRoute()) {
        self.router = router
    }
}

// MARK: - MediaNetworkingProtocol
extension MediaServicesManager: MediaServicesManagerProtocol {
    func getImage(fromUrl url: String, completion: @escaping (Result<UIImage, String, ErrorResult>) -> Void) {
        let image = imageCache.object(forKey: url as NSString)
        guard image == nil else {//Check catch
            completion(.success(image!, url))
            return
        }
        
        get(fromUrl: url) { result in
            switch result {
            case .failure(let error):
                completion(.failure(error))
            case .success(let data, let response):
                guard let responseUrl = response.url?.absoluteString, let image = UIImage(data: data) else {
                    completion(.failure(.network(string: NetworkResponse.nilResponse.rawValue)))
                    return
                }
                self.imageCache.setObject(image, forKey: responseUrl as NSString)
                completion(.success(image, responseUrl))
            }   
        }
    }
    
    func isResponseValid(askedUrl: String?, imageUrl: String) -> Bool {
        guard let askedUrl = askedUrl else{return false}
        return askedUrl == imageUrl
    }
    func cleanCache() {
        imageCache.removeAllObjects()
    }
}


// MARK: - Private methods
extension MediaServicesManager {
    private func get(fromUrl url: String, completion: @escaping (Result<Data, URLResponse, ErrorResult>) -> Void) {
        router.request(withUrl: url) {data, response, error in
            guard error == nil else {
                completion(.failure(.network(string: error.debugDescription)))
                return
            }
            guard let data = data, let response = response as? HTTPURLResponse else {
                completion(.failure(.network(string: NetworkResponse.failToCastResponse.rawValue)))
                return
            }
            completion(NetworkResponseUtils().handleNetworkResponse(data: data, response: response))
        }
    }
}
