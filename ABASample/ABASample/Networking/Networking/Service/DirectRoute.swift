//
//  DirectRoute.swift
//  gGmbH
//
//  Created by Alejandro Gelos on 12/4/18.
//  Copyright © 2018 Alejandro Gelos. All rights reserved.
//

import Foundation

final class DirectRoute {
    private var task: URLSessionTask?
    
    private lazy var queue: OperationQueue = {
        var queue = OperationQueue()
        queue.qualityOfService = .background
        queue.name = "Direct router queue"
        return queue
    }()
    
    func request(withUrl url: String, completion: @escaping NetworkRouterCompletion) {
        queue.addOperation {[weak self] in
            guard let `self` = self else {return}
            let session = URLSession.shared
            do {
                let request = try self.buildRequest(fromUrl: url)
                NetworkLogger.log(request: request)
                self.task = session.dataTask(with: request, completionHandler: { data, response, error in
                    NetworkingUtils.activeNetworkProcess -= 1
                    completion(data, response, error)
                })
            } catch {
                NetworkingUtils.activeNetworkProcess -= 1
                completion(nil, nil, error)
            }
            self.task?.resume()
            NetworkingUtils.activeNetworkProcess += 1
        }
    }
    
    func cancel() {
        queue.addOperation {[weak self] in
            self?.task?.cancel()
        }
    }
}

// MARK: - Private methods
extension DirectRoute {
    private func buildRequest(fromUrl url: String) throws -> URLRequest {
        guard let url = URL(string: url) else {throw NetworkResponse.badRequest}
        return URLRequest(url: url,
                             cachePolicy: .reloadIgnoringLocalAndRemoteCacheData,
                             timeoutInterval: 10.0)
    }
}
