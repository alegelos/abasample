//
//  NetworkingUtils.swift
//  gGmbH
//
//  Created by Alejandro Gelos on 12/4/18.
//  Copyright © 2018 Alejandro Gelos. All rights reserved.
//

import UIKit

struct NetworkingUtils {
    static var activeNetworkProcess = 0 {
        didSet {
            DispatchQueue.main.async {            
                UIApplication.shared.isNetworkActivityIndicatorVisible = activeNetworkProcess > 0
            }
        }
    }
}
