//
//  iTunesErrors.swift
//  gGmbH
//
//  Created by Alejandro Gelos on 12/3/18.
//  Copyright © 2018 Alejandro Gelos. All rights reserved.
//

import Foundation

public typealias NetworkRouterCompletion = (_ data: Data?,_ response: URLResponse?,_ error: Error?)->()

enum Result<T, U, E: Error> {
    case success(T, U)
    case failure(E)
}

enum ErrorResult: Error {
    case network(string: String)
    case parser(string: String)
    case custom(string: String)
}

enum NetworkResponse:String, Error {
    case success
    case authenticationError = "You need to be authenticated first."
    case badRequest = "Bad request"
    case outdated = "The url you requested is outdated."
    case failed = "Network request failed."
    case failToCastResponse = "Fail to cast response"
    case noData = "Response returned with no data to decode."
    case unableToDecode = "We could not decode the response."
    case nilResponse = "Request response is nil"
}

struct NetworkResponseUtils {
    func handleNetworkResponse(data:Data?, response: URLResponse?) -> Result<Data, URLResponse, ErrorResult> {
        guard let data = data else {
            return .failure(.network(string: NetworkResponse.noData.rawValue))
        }
        guard let response = response as? HTTPURLResponse else {
            return .failure(.network(string: NetworkResponse.failToCastResponse.rawValue))
        }
        
        switch response.statusCode {
        case 200...299: return .success(data, response)
        case 401...500: return .failure(.network(string: NetworkResponse.authenticationError.rawValue))
        case 501...599: return .failure(.network(string: NetworkResponse.badRequest.rawValue))
        case 600: return .failure(.network(string: NetworkResponse.outdated.rawValue))
        default: return .failure(.network(string: NetworkResponse.failed.rawValue))
        }
    }
}
