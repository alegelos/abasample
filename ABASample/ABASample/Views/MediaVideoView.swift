//
//  MediaVideoView.swift
//  ABASample
//
//  Created by Alejandro Gelos on 12/11/18.
//  Copyright © 2018 Alejandro Gelos. All rights reserved.
//

import UIKit
import AVFoundation

final class MediaVideoView: UIView {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        if let layer = layer as? AVPlayerLayer {
            layer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        }
    }
    
    override class var layerClass: AnyClass {
        return AVPlayerLayer.self
    }
}
