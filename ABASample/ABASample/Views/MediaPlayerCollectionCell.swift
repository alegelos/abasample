//
//  MediaPlayerCollectionCell.swift
//  gGmbH
//
//  Created by Alejandro Gelos on 12/7/18.
//  Copyright © 2018 Alejandro Gelos. All rights reserved.
//

import UIKit
import AVFoundation

final class MediaPlayerCollectionCell: UICollectionViewCell {
    @IBOutlet weak var trackCoverImage: UIImageView!
    @IBOutlet weak var trackVideoView: MediaVideoView!
    @IBOutlet weak var trackTitle: UILabel!
    @IBOutlet weak var trackArtist: UILabel!
    @IBOutlet weak var loadingIndicator: LoadingIndicator!
    
    private var mediaUrl:      String?
    private var coverImageUrl: String?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateVideoLayer(notification:)), name: NSNotification.Name.willPlayMedia, object: nil)
    }
    
    func loadCell(withTrack track: Track, mediaManager: MediaServicesManagerProtocol) {
        loadFirstLabel (withTrack: track)
        loadSecondLabel(withTrack: track)
        mediaUrl = track.previewUrl
        
        if let coverImageUrl = track.artworkUrl100{
            trackCoverImage.image = nil
            self.coverImageUrl = coverImageUrl
            loadCoverImage(withMediaManager: mediaManager)
        }
        
        if mediaUrl == MediaPlayerManager.shared.getCurrentTrackData().previewUrl{
            set(videoPlayer: MediaPlayerManager.shared.getPlayer())
        } else{
            set(videoPlayer: nil)
        }
    }
    
    func set(videoPlayer: AVPlayer?) {
        DispatchQueue.main.async { [weak self] in
            guard let layer = self?.trackVideoView.layer as? AVPlayerLayer else {return}
            layer.player = videoPlayer
        }
    }
}

// MARK - Private Methods
extension MediaPlayerCollectionCell{
    
    private func loadFirstLabel(withTrack track: Track) {
        trackTitle.text = track.trackName
    }
    
    private func loadSecondLabel(withTrack track: Track) {
        trackArtist.text = track.artistName
    }
    
    private func loadCoverImage(withMediaManager mediaManager: MediaServicesManagerProtocol) {
        guard let coverImageUrl = coverImageUrl else {return}
        loadingIndicator.startAnimating()
        
        mediaManager.getImage(fromUrl: coverImageUrl) { [weak self] result in
            guard let `self` = self else {return}
            self.loadingIndicator.stopAnimating()
            
            switch result {
            case .failure(let error):
                print(error)
            case .success(let image, let url):
                guard mediaManager.isResponseValid(askedUrl: self.coverImageUrl, imageUrl: url) else {return}//Discard images from old request
                DispatchQueue.main.async {
                    self.trackCoverImage.image = image
                }
            }
        }
    }
    
    @objc private func updateVideoLayer(notification: Notification){
        guard let userInfo = notification.userInfo, let mediaUrl = userInfo["track"] as? String, mediaUrl == self.mediaUrl else {
            set(videoPlayer: nil)
            return
        }
        set(videoPlayer: MediaPlayerManager.shared.getPlayer())
    }
}
